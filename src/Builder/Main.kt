package Builder

fun main() {

    val pizzaVegetariana = Pizza.Builder()
        .masa("gruesa")
        .quesoExtra(true)
        .pedazos(8)
        .vegetal("champiñones")
        .build()

    val pizzaPepperoni = Pizza.Builder()
        .masa("delgada")
        .pedazos(12)
        .carne("Peperonni")
        .condimento("Orégano")
        .build()

    println("********Pizza Vegetariana********")
    println(pizzaVegetariana.imprimitPizza())
    println("\n********Pizza Pepperoni********")
    println(pizzaPepperoni.imprimitPizza())
}