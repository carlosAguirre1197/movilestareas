package Builder

class Pizza (
    val masa: String?,
    val quesoExtra: Boolean?,
    val pedazos: Number?,
    val carne: String?,
    val condimento: String?,
    val vegetal: String?
){
    data class Builder(
        var masa: String? = null,
        var quesoExtra: Boolean? = false,
        var pedazos: Number? = 6,
        var carne: String? = null,
        var condimento: String? = null,
        var vegetal: String? = null
    ){
        fun masa(masa: String) = apply {this.masa = masa}
        fun quesoExtra(quesoExtra: Boolean) = apply {this.quesoExtra = quesoExtra}
        fun pedazos(pedazos: Number) =  apply {this.pedazos = pedazos}
        fun carne(carne: String) = apply {this.carne = carne}
        fun condimento(condimento: String) = apply {this.condimento = condimento}
        fun vegetal(vegetal: String) = apply {this.vegetal = vegetal}
        fun build() = Pizza(masa, quesoExtra, pedazos, carne, condimento, vegetal)

    }

    fun imprimitPizza(): String {return "Masa: "+masa+
            "\nQueso Extra: "+quesoExtra+
            "\nPedazos: "+pedazos+
            "\nCarne: "+carne+
            "\nCondimento: "+condimento+
            "\nVegetal: "+vegetal}
}
