package Factory

enum class GeneroVideojuego {  RPG, Shooter }

class RPG(override val nombre: String, override val precio:Double):Videojuegos{
    override fun genero()="RPG"
}

class Shooter(override val nombre: String, override val precio:Double):Videojuegos{
    override fun genero()="Shooter"
}