package Factory

fun main() {

    val rpg = VideojuegoFactory.getVideojuego(GeneroVideojuego.RPG, "Pokemon", 60.00)
    with(rpg){
    print(imprimirDatos())
    println(genero())
    }


    val shooter = VideojuegoFactory.getVideojuego(GeneroVideojuego.Shooter, "CoD", 90.00)
    with(shooter){
        print(imprimirDatos())
        println(genero())
    }

}