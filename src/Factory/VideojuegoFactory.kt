package Factory

object VideojuegoFactory {
    fun getVideojuego(generoVideojuego: GeneroVideojuego, nombre:String, precio:Double):Videojuegos{
        return when (generoVideojuego){
            GeneroVideojuego.RPG -> RPG(nombre = nombre, precio = precio)
            GeneroVideojuego.Shooter -> Shooter(nombre = nombre, precio = precio)
        }
    }
}