package Decorator

open class DecoradorHelado(protected  var helado:Helado) : Helado {
    override fun probarHelado() {
        this.helado.probarHelado()
    }
}