package Decorator

class HeladoDuraznos(helado:Helado) : DecoradorHelado(helado) {

    override public fun probarHelado() {
        super.probarHelado();
        this.añadirComplemento();
        println(" Tiene duraznos");
    }

    public fun añadirComplemento(){
        println("Añadiendo duraznos al helado");
    }
}