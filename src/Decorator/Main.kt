package Decorator

fun main(args: Array<String>) {
    val heladoNatural = HeladoConcreto()
    heladoNatural.probarHelado()
    print("\n");
    val heladoConDuraznos = HeladoDuraznos(HeladoConcreto())
    heladoConDuraznos.probarHelado()
    val heladoConFrutillas = HeladoFrutillas(HeladoConcreto())
    heladoConFrutillas.probarHelado()
}