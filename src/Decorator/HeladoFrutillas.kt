package Decorator

class HeladoFrutillas(helado:Helado) : DecoradorHelado(helado) {

    override public fun probarHelado() {
        super.probarHelado();
        this.añadirComplemento();
        println(" Tiene frutilas");
    }

    public fun añadirComplemento(){
        println("Añadiendo frutillas al helado");
    }
}