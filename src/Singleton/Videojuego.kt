package Singleton

class Videojuego private constructor(){


    init { println("Este ($this) es un videojuego") }

    private object Holder { val INSTANCE = Videojuego() }

    companion object {
        val instance: Videojuego by lazy { Holder.INSTANCE }
    }
    var b:String? = null

}