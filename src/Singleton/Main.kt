package Singleton

fun main() {

    var first = Videojuego.instance
    println(first.b)

    var second = Videojuego.instance
    second.b = "God of War"
    println(second.b)

    var third = Videojuego.instance
    println(third.b)
}