package FactoryRepaso

fun main() {

    val rpg = VideojuegoFactory.getVideojuego("RPG", "Pokemon", 60.00)
    with(rpg){
        print(imprimirDatos())
        println(genero())
    }


    val shooter = VideojuegoFactory.getVideojuego("Shooter", "CoD", 90.00)
    with(shooter){
        print(imprimirDatos())
        println(genero())
    }

}