package FactoryRepaso

interface Videojuegos {
    val nombre: String
    val precio: Double
    fun genero(): String
    fun imprimirDatos() = "Nombre: $nombre\nPrecio: $precio\n"
}