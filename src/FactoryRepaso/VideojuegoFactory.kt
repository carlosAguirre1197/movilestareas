package FactoryRepaso

object VideojuegoFactory {
    fun getVideojuego(genero:String, nombre:String, precio:Double):Videojuegos{
        return when (genero){
            "RPG" -> RPG(nombre = nombre, precio = precio)
            "Shooter" -> Shooter(nombre = nombre, precio = precio)
            else -> throw Exception("I don't know how to deal with it")
        }
    }
}