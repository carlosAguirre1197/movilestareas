package Observer

import kotlin.properties.Delegates

class observadorTexto {
    var escuchador: EscuchadorCambioDeTexto? = null
    var texto: String by Delegates.observable("") {
        prop, old, new -> escuchador?.textoCambiado(new)
    }
}