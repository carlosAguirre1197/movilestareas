package Observer

fun main() {

    val objetoObserador = observadorTexto()
    objetoObserador.escuchador = ImpresoraDeTextoCambiado()
    objetoObserador.texto = "Hola"
    objetoObserador.texto = "¿Cómo estás?"
}