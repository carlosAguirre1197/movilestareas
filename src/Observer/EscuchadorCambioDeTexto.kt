package Observer

interface EscuchadorCambioDeTexto {

    fun textoCambiado(textoNuevo: String)

}