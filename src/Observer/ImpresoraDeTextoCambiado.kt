package Observer


class ImpresoraDeTextoCambiado : EscuchadorCambioDeTexto {

    override fun textoCambiado(textoNuevo: String) = println("El texto es cambiado por: $textoNuevo")

}